<?php
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;
use Firebase\JWT\Key;
$app = new \Slim\App;
error_reporting(E_ALL);
ini_set('display_errors', 1);





// FINDER 1 : bonjour
$app->get('/bonjour', function(Request $request, Response $response){
  return "bonjour";
});





// FINDER 2 : chambre id
$app->get('/chambre_id/{id}', function(Request $request, Response $response){
  $id = $request->getAttribute('id');
       return getChambre($id);
});
function connexion()
{
   return $dbh = new PDO("mysql:host=localhost;dbname=u440088550_portfolio", 'u440088550_mohamed', 'Mohazer_7', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getChambre($id)
{
  $sql = "SELECT * FROM chambre WHERE id=$id";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
     $result = $statement->fetchAll(PDO::FETCH_CLASS);
                 return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}





// EPREUVE BLANCHE : chambre vue

$app->get('/chambre_vue/{vue}', function(Request $request, Response $response){
  $vue = $request->getAttribute('vue');
       return getChambreVue($vue);
});
function getChambreVue($vue)
{
  $sql = "SELECT * FROM chambre WHERE vue='$vue'";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
     $result = $statement->fetchAll(PDO::FETCH_CLASS);
                 return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}





// FINDER 3 : Connexion user

$app->get('/connexion', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $mail = $tb["mail"];
  $password = $tb["password"];
 return checkUser($mail, $password);
});
function checkUser($mail, $password)
{
 $sql = "SELECT * FROM user where mail=? AND password=?";
 try{
   $dbh=connexion();
   $statement = $dbh->prepare($sql);
   $statement->execute(array($mail, $password));
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    if($result){
      return json_encode($result, JSON_PRETTY_PRINT);
    }
 } catch(PDOException $e){
   return '{"error":'.$e->getMessage().'}';
 }
}





// FINDER 3 : Inscription user

$app->get('/inscription', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
  $nom = $tb["nom"];
  $prenom = $tb["prenom"];
  $mail = $tb["mail"];
  $password = $tb["password"];
 return insertUser($nom, $prenom, $mail, $password);
});

function insertUser($nom, $prenom, $mail, $password)
{
  $sql = "INSERT INTO user(nom, prenom, mail, password) VALUES (?, ?, ?, ?)";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute(array($nom, $prenom, $mail, $password));
      $result = $statement->fetchAll(PDO::FETCH_CLASS);
      if ($result){
        return "Inscription validée";
      }else{
        return "Mauvais paramètres ou compte existant";
      }
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}





// FINDER 4 : Supprimer user

$app->get('/suppression', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
        $mail = $tb["mail"];
        $password = $tb["password"];
 return deleteUser($mail, $password);
});
function deleteUser($mail, $password)
{
 $sql = "DELETE FROM user WHERE mail='$mail' AND password='$password'";
 try{
   $dbh=connexion();
   $statement = $dbh->prepare($sql);
   $statement->execute(array($mail, $password));
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    if ($result){
      return "Suppression validée";
    }else{
      return "Compte inexistant";
    }
 } catch(PDOException $e){
   return '{"error":'.$e->getMessage().'}';
 }
}





// FINDER 4 : Modifier mail

$app->get('/modification', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
        $newmail = $tb["newmail"];
        $oldmail = $tb["oldmail"];
        $password = $tb["password"];
 return modifUser($newmail, $oldmail, $password);
});
function modifUser($newmail, $oldmail, $password)
{
 $sql = "UPDATE user SET mail='$newmail' WHERE mail='$oldmail' AND password='$password'";
 try{
   $dbh=connexion();
   $statement = $dbh->prepare($sql);
   $statement->execute(array($newmail, $oldmail, $password));
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
                return "Modication de l'ancienne adresse mail '$oldmail' en '$newmail'";
 } catch(PDOException $e){
   return '{"error":'.$e->getMessage().'}';
 }
}





// FINDER 4 : Afficher toutes les chambres

$app->get('/chambres', function(Request $request, Response $response){
       return getAllChambres();
});
function getAllChambres()
{
  $sql = "SELECT * FROM chambre";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
     $result = $statement->fetchAll(PDO::FETCH_CLASS);
                 return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}






// pré FINDER 5

function tokenUser($mail, $password)
{
 $sql = "SELECT * FROM user where mail=? AND password=?";
 try{
   $dbh=connexion();
   $statement = $dbh->prepare($sql);
   $statement->execute(array($mail, $password));
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
      if ($result){
        return true;
      } else{
        return false;
      }
 } catch(PDOException $e){
   return '{"error":'.$e->getMessage().'}';
 }
}

// FINDER 5 :

$app->get('/obtentionToken', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
        $mail = $tb["mail"];
        $password = $tb["password"];
  $allowed = tokenUser($mail, $password);
  if($allowed){
    $token = getTokenJWT();
    return $response->withJson($token, 200);
  }else{
    return $response->withStatus(401);
  }
});


$app->get('/verifToken', function(Request $request, Response $response){
  $tb = $request->getQueryParams();
      $token = $tb["token"];
  if(validJWT($token)){
    //J'execute la fonction
  }else{
    //non autorisé
    return $response->withStatus(401);
  }
});
 function getTokenJWT() {
  $payload = array("exp" => time() + (60 * 30));  // 30 min
    // encode the payload using our secretkey and return the token
  return JWT::encode($payload, "secret", "HS256");
}
function validJWT($token) {
  $res = false;
  try {
      $decoded = JWT::decode($token, new Key("secret", 'HS256'));
  } catch (Exception $e) {
    return $res;
  }
  $res = true;
  return $res;
}


$app->run();